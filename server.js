let express = require('express');
let app = express();


app.get('/', function (req, res) {
    res.json({ 
        products: [{
            name: "laptop", 
            price: 500
        }, {
            name: "monitor",
            price: 120
        }, {
            name: "keyboard",
            price: 99
        }]
    });
    res.end();
});

app.listen(3001, function () {
    console.log("Hello world")
    console.log("Products listening on port 3001!");
});

